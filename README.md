# Insight

1. Build the Look and Feel of Insight (with provided mocks) using and Angular and Sass.
2. Show player information from API (optional)

## Prerequisites

1. [Bower](http://bower.io/)
2. [Angular](https://angular.io/)
3. [Materialize](http://materializecss.com/getting-started.html).
4. [Gulp](http://gulpjs.com/).
5. [Node](https://nodejs.org/en/).

## Build & development

The Main goal is to see your coding and troubleshooting skills.
User API:
http://backoffice.insight.cloud/api/userInfo/getUserInfo
http://backoffice.insight.cloud/api/wallet/getBalance
